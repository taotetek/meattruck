CFLAGS="-fPIC -fsanitize=thread"

colony_proto:
	$(CC) -o meattruck meattruck.c meattrucklib.c bstrlib.c -I. -I/usr/include/json-c -I/usr/local/include -I/usr/local/include/json-c -lczmq -ljson-c -lm -lpqtypes -lpq -ljson -g -Wall

clean:
	rm -f meattruck
