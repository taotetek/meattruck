#include <signal.h>
#include <czmq.h>
#include <json.h>
#include <libpq-fe.h>
#include <libpqtypes.h>
#include <math.h>
#include "ansi_colors.h"
#include "dbg.h"
#include "bstrlib.h"

#define MINUTE 60
#define QUARTER_HOUR 900
#define HALF_HOUR 1800
#define HOUR 3600
#define DAY 86400

typedef struct {
    PGconn *pg_conn;
    PGresult *pg_res;
    zlist_t *input_addrs;
    void *input;
    int debug;
} meattruck_t;

long 
round_time(long unixts, int interval_seconds);

PGtimestamp
get_pg_timestamp (time_t epoch);

void 
sig_handler (int signum);

int
update_interval (meattruck_t *truck, const char *topic, const char *fingerprint, time_t epoch);

meattruck_t *
meattruck_new (zctx_t *ctx);

void
meattruck_destroy (meattruck_t **self_p);
