#include "meattruck.h"

static int
s_handle_event (zloop_t *loop, zmq_pollitem_t *poller, void *arg)
{
    meattruck_t *truck = (meattruck_t *) arg;

    zmsg_t *msg = zmsg_recv (truck->input);

    if (msg) {
        zframe_t *t_frame = zmsg_unwrap (msg);
        char *topic = zframe_strdup (t_frame);
        zframe_destroy (&t_frame);

        zframe_t *s_frame = zmsg_unwrap (msg);
        char *stat = zframe_strdup (s_frame);
        zframe_destroy (&s_frame);

        json_object *stat_obj = json_tokener_parse (stat);
        
        json_object *epoch_obj;
        if (json_object_object_get_ex (stat_obj, "epoch_ms", &epoch_obj)) {
            json_object *finger_obj;
            if (json_object_object_get_ex (stat_obj ,"fingerprint", &finger_obj)) {
                const char *fingerprint = json_object_get_string (finger_obj);
                time_t epoch = json_object_get_int64 (epoch_obj);
                int rc = update_interval (truck, topic, fingerprint, epoch);
                assert (rc == 0);
            }
        }

        json_object_put (stat_obj);
        zmsg_destroy (&msg);
    }
    return 0;
}

int
main (void)
{
    zctx_t *ctx = zctx_new ();
    signal (SIGINT, sig_handler);
    meattruck_t *truck = meattruck_new (ctx); 
    if (!truck)
        goto failed;
   
    zloop_t *loop = zloop_new ();
    if (!loop)
        goto failed;
    
    zloop_set_verbose (loop, 0);
    zmq_pollitem_t poll_input = { truck->input, 0, ZMQ_POLLIN };
    int rc = zloop_poller (loop, &poll_input, s_handle_event, truck);
    if (rc !=0 )
        goto failed;

    zloop_start (loop);
    zloop_destroy (&loop);
    zctx_destroy (&ctx);
    meattruck_destroy (&truck);
    
    return EXIT_SUCCESS;
failed:
    log_debug ("FAILED");
    meattruck_destroy (&truck);
    return EXIT_FAILURE;
}
