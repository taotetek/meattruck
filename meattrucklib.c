#include "meattruck.h"

void
sig_handler (int signum)
{
    exit (signum);
}

long 
round_time(long unixts, int interval_seconds)
{
    long interval = (floor(unixts / interval_seconds) * interval_seconds);
    return interval;
}

PGtimestamp
get_pg_timestamp (time_t epoch)
{
    epoch = epoch / 1000;
    time_t interval = round_time (epoch, QUARTER_HOUR);
   
    log_debug ("INTERVAL: %" PRId64, interval);
                
    struct tm *ts = localtime(&interval);
                
    PGtimestamp pg_ts;
    pg_ts.date.isbc = 0;
    pg_ts.date.year = 1900 + ts->tm_year;
    pg_ts.date.mon = ts->tm_mon;
    pg_ts.date.mday = ts->tm_mday;
    pg_ts.time.hour = ts->tm_hour;
    pg_ts.time.min = ts->tm_min;
    pg_ts.time.sec = ts->tm_sec;
    pg_ts.time.usec = 0;

    return pg_ts;
}

int
update_interval (meattruck_t *truck, const char *topic, const char *fingerprint, time_t epoch)
{
    PGtimestamp pg_ts = get_pg_timestamp (epoch);
                
    truck->pg_res = PQexec (truck->pg_conn, "BEGIN");
    PQclear (truck->pg_res);

    truck->pg_res = PQexecf (truck->pg_conn,
        "SELECT COUNT(*) AS count FROM daily_stats "
        "WHERE interval = %timestamp AND topic = %text",
        &pg_ts, topic);

    if (!truck->pg_res)
        fprintf (stderr, "ERROR: %s\n", PQgeterror());
                
    PGint8 count;
    PQgetf (truck->pg_res, 0, "%int8", 0, &count);
    printf ("count: %" PRId64 "\n", (long)count);
    PQclear (truck->pg_res);
    if (count == 0) {
        fprintf (stderr, "count was 0\n");
        truck->pg_res = PQexecf (truck->pg_conn,
            "INSERT INTO daily_stats (interval, topic, users, total) "
            "VALUES (%timestamp, %text, hll_empty(), 0)",
            &pg_ts, topic);

        if (!truck->pg_res)
            fprintf (stderr, "ERROR: %s\n", PQgeterror());
   
        PQclear (truck->pg_res);
    }

    truck->pg_res = PQexecf (truck->pg_conn,
        "UPDATE daily_stats SET total = total + 1, "
        "users = hll_add(users, hll_hash_text(%text)) "
        "WHERE topic = %text AND interval = %timestamp",
        fingerprint, topic, &pg_ts);

    if (!truck->pg_res)
        fprintf (stderr, "ERROR: %s\n", PQgeterror());

    truck->pg_res = PQexec (truck->pg_conn, "COMMIT");
    return 0;
}


meattruck_t *
meattruck_new (zctx_t *ctx)
{
    meattruck_t *self = (meattruck_t *) zmalloc (sizeof (meattruck_t));
    self->debug = 1;
    zconfig_t *conf = zconfig_load ("./meattruck.cfg");
    
    char *pg_conn_string = zconfig_resolve(conf, "main/pg", NULL);
    self->pg_conn = PQconnectdb(pg_conn_string);
    free (pg_conn_string);

    if (PQstatus(self->pg_conn) != CONNECTION_OK) {
        log_failed ("Could not connect to postgresql");
        goto failed;
    }
    PQinitTypes (self->pg_conn);

    self->forward = zsocket_new (ctx, ZMQ_PUB);
    int rc = zsocket_bind (self->forward, "ipc:///tmp/forward_truck");
    if (rc == -1) {
        log_failed ("Could not bind zeromq forward address");
        goto failed;
    }

    self->input = zsocket_new (ctx, ZMQ_SUB);
    zsocket_set_subscribe (self->input, "");
    
    char *buffer = zconfig_resolve (conf, "main/zmq", NULL);
    bstring input_str = bfromcstr (buffer);
    free (buffer);
    struct bstrList *inputs;
    if (NULL != (inputs = bsplit (input_str, ';'))) {
        int i;
        for (i=0; i<inputs->qty; i++) {
            int rc = zsocket_bind (self->input, bdata(inputs->entry[i]));
            if (rc == -1) {
                log_failed ("Could not bind zeromq input address");
                goto failed;
            }
        }
        bstrListDestroy (inputs);
        bdestroy (input_str);
    }
    else {
        log_failed ("Could not find input addresses in config");
        goto failed;
    }
    return self;
failed:
    return NULL;
}

void
meattruck_destroy (meattruck_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        meattruck_t *self = *self_p;
        PQfinish (self->pg_conn);
        free (self);
        *self_p = NULL;
    }
}


